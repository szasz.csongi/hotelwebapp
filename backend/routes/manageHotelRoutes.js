import express from 'express';

import hotels from './inputData.js';
import { check, insertHotel, insertRooms, selectNearbyHotels, truncateHotels, truncateRooms } from '../db/manageHotelsDb.js';
import { latLonToMeters } from '../utils/latLonToMeters.js';

const router = express.Router();

router.get('/uploadData', async (req, res) => {
    const hotelCommands = [];
    const roomCommands = [];
    try {
        
        hotels.forEach(hotel => {
            const hotelPositionInMeters = latLonToMeters(hotel.latitude, hotel.longitude);
            const hotelData = {
                name: hotel.name,
                x: hotelPositionInMeters.x,
                y: hotelPositionInMeters.y
            };
            
            hotelCommands.push(insertHotel(hotelData));

            hotel.rooms.forEach(room => {
                const roomData = {
                    hotelName: hotel.name,
                    roomNumber: room.roomNumber,
                    price: room.price,
                    isAvailable: room.isAvailable
                }
                roomCommands.push(insertRooms(roomData));
            })
        });

        await Promise.all(hotelCommands);
        await Promise.all(roomCommands);

    } catch (err) {
        res.write('Error in uploading the data', err);
        res.status(500);
        return res.end();
    }
    return res.redirect('/');
});

router.post('/getRadiusHotels', async(req, res) => {

    console.log(req.body);
    const userPosition = latLonToMeters(req.body.latitude, req.body.longitude);
    const distance = req.body.distance * 1000;
    const data = {
        x: userPosition.x,
        y: userPosition.y,
        distance: distance
    }

    try{
        const hotels = await selectNearbyHotels(data);
        return res.send(hotels);
    } catch(err) {
        res.write('Error in fetching hotels', err);
        res.status(500);
        return res.end();
    }
})

export default router;