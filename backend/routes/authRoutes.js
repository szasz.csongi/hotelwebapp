import express from 'express';
import { insertUser, loginUser } from '../db/authDb.js';

const router = express.Router();

router.post('/register', async(req, res) => {
    try{
        await insertUser(req.body);
        return res.redirect('/');
    } catch(err) {
        res.write('Error in register', err);
        res.status(500);
        return res.end();
    }
})

router.post('/login', async (req, res) => {
    try {
      const user = await loginUser(req.body);
      if (user) {
        res.status(200).send({ success: true, user });
      } else {
        res.status(401).send({ success: false, error: 'Invalid credentials' });
      }
    } catch (err) {
        res.write('Error in login', err);
        res.status(500);
        return res.end();
    }
  });

export default router;