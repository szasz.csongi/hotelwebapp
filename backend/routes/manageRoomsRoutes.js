import express from 'express';
import { bookRoom, cancelRoom, selectRoomsByHotelName } from '../db/manageRoomsDb.js';

const router = express.Router();

router.post('/getHotelRooms', async(req, res) => {
    const hotelName = req.body.hotelName;
    try {
        const rooms = await selectRoomsByHotelName(hotelName);
        return res.send(rooms);
    } catch(err) {
        res.write('Error in fetching rooms', err);
        res.status(500);
        return res.end();
    }
})

router.post('/book', async(req,res) => {
    try {
        await bookRoom(req.body);
    } catch(err) {
        res.write('Error in updating rooms', err);
        res.status(500);
        return res.end();
    }
})

router.post('/cancelBooking', async(req,res) => {
    try {
        await cancelRoom(req.body.roomId);
    } catch(err) {
        res.write('Error in updating rooms', err);
        res.status(500);
        return res.end();
    }
})

export default router;