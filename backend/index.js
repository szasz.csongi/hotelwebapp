import express from 'express';
import morgan from 'morgan';
import cors from 'cors';
import bp from 'body-parser';
import cookieParser from 'cookie-parser';

import manageHotelRoutes from './routes/manageHotelRoutes.js';
import manageRoomsRoutes from './routes/manageRoomsRoutes.js';
import authRoutes from './routes/authRoutes.js';

const app = express();
const PORT = process.env.PORT || 5000;

app.use(morgan('tiny'));
app.use(express.json());
app.use(cookieParser());
app.use(bp.json({limit: "50mb"}))
app.use(bp.urlencoded({limit: "50mb", extended: true, parameterLimit:50000 }))
app.use(cors({
  origin: 'http://localhost:3000'
}));

app.get('/', (req, res) => {
    res.send('Hello from my Express!');
});

app.use('/manageHotel', manageHotelRoutes);
app.use('/manageRooms', manageRoomsRoutes);
app.use('/authRoutes', authRoutes);

app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
});
