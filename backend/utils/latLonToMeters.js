
export function latLonToMeters(lat, lon) {
    // Earth radius in meters
    const R = 6378137;

    // Convert latitude and longitude from degrees to radians
    const radLat = lat * Math.PI / 180;
    const radLon = lon * Math.PI / 180;

    // Using the Mercator projection formula
    const x = R * radLon;
    const y = R * Math.log(Math.tan(Math.PI / 4 + radLat / 2));

    return { x, y };
}

// Example usage
const userLat = 37.7749; // Latitude of San Francisco
const userLon = -122.4194; // Longitude of San Francisco

const userPositionInMeters = latLonToMeters(userLat, userLon);

