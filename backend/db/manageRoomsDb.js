import callQuery from './connection.js';

export function selectRoomsByHotelName(hotelname) {
    const queryStr = `
        SELECT *
        FROM
            rooms
        WHERE
            hotelname = ?`;

    return callQuery(queryStr, [hotelname]);
}

export function bookRoom(data) {
    const queryStr = `UPDATE rooms
        SET bookedBy = ?, isAvailable = 0 
        WHERE roomid = ?`;
    return callQuery(queryStr, [data.username, data.roomId])
}

export function cancelRoom(roomId) {
    const queryStr = `UPDATE rooms
        SET bookedBy = '', isAvailable = 1 
        WHERE roomid = ?`;
    return callQuery(queryStr, [roomId])
}