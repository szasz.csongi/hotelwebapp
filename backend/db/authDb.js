import callQuery from './connection.js';

export function insertUser(data) {
    const queryStr = `INSERT INTO users
    VALUES (default, ?, ?)`;
    return callQuery(queryStr, [data.username,
        data.password]);
}

export function loginUser(data) {
    const queryStr = 'SELECT * FROM users WHERE users.username = ? AND users.password = ?';
    return callQuery(queryStr, [data.username, data.password]).then((user) => user[0]);
}