import callQuery from './connection.js';

export function insertHotel(data) {
    const queryStr = `INSERT INTO hotels
    VALUES (default, ?, ?, ?)`;
    return callQuery(queryStr, [data.name,
        data.x, data.y]);
}

export function insertRooms(data) {
    const queryStr = `INSERT INTO rooms
    VALUES (default, ?, ?, ?, ?, '')`;
    return callQuery(queryStr, [data.hotelName,
        data.roomNumber, data.price, data.isAvailable]);
}

export function check() {
    const checkTableExistsQuery = `
        SELECT COUNT(*) AS tableCount
        FROM information_schema.tables
        WHERE table_schema = DATABASE()
        AND table_name = 'hotels'`;

    return callQuery(checkTableExistsQuery)
}

export function truncateHotels() {
    const queryStr = 'TRUNCATE TABLE hotels';
    return callQuery(queryStr);
}

export function truncateRooms() {
    const queryStr = 'TRUNCATE TABLE rooms';
    return callQuery(queryStr);
}

export function selectNearbyHotels(data) {
    const queryStr = `
        SELECT hotelname, x, y 
        FROM
            hotels
        WHERE
            SQRT(POW(x - ?, 2) + POW(y - ?, 2)) < ?`;

    return callQuery(queryStr, [data.x, data.y, data.distance]);
}