import mysql from 'mysql';
import util from 'util';

const connectionPool = mysql.createPool({
    connectionLimit: 10,
    database: 'hotel',
    host: 'localhost',
    port: 3306,
    user: 'root',
    password: 'Csongi123csongi',
});

const callQuery = util.promisify(connectionPool.query).bind(connectionPool);

export default callQuery;

function createHotel() {
    return callQuery(`CREATE TABLE IF NOT EXISTS hotels (
        hotelid INT PRIMARY KEY AUTO_INCREMENT,
        hotelname VARCHAR(255),
        x DOUBLE,
        y DOUBLE
    )`);
}

function createRooms() {
    return callQuery(`CREATE TABLE IF NOT EXISTS rooms (
        roomid INT PRIMARY KEY AUTO_INCREMENT,
        hotelname VARCHAR(255),
        roomNumber INT,
        price INT,
        isAvailable BOOL,
        bookedBy VARCHAR(255)
    )`);
}

function createUsers() {
    return callQuery(`CREATE TABLE IF NOT EXISTS users (
        userid INT PRIMARY KEY AUTO_INCREMENT,
        username VARCHAR(255),
        password VARCHAR(255)
    )`)
}

(async () => {
    try {
        await createHotel();  
        await createRooms(); 
        await createUsers();
    } catch (err) {
        console.error(err);
    }
})();
