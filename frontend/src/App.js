import React, { useState, useEffect } from 'react';
import axios from 'axios';
import './App.css';
import Cookies from 'js-cookie';

import RegisterButton from './components/Register';
import LoginButton from './components/Login';

function App() {
  const [distance, setDistance] = useState('');
  const [hotels, setHotels] = useState([]);
  const [rooms, setRooms] = useState({});
  const [selectedHotel, setSelectedHotel] = useState(null);

  const [user, setUser] = useState(null);

  useEffect(() => {
    const cookieUser = Cookies.get('user');
    if (cookieUser) {
      setUser(JSON.parse(cookieUser));
    }
  }, []);

  const handleLogout = () => {
    Cookies.remove('user');
    setUser(null);
  };


  const handleGetHotels = () => {
    if ("geolocation" in navigator) {
      navigator.geolocation.getCurrentPosition(async function(position) {
        const latitude = position.coords.latitude;
        const longitude = position.coords.longitude;
        
        try {
          const response = await axios.post('http://localhost:5000/manageHotel/getRadiusHotels', {
            latitude: latitude,
            longitude: longitude,
            distance: distance
          });
          setHotels(response.data);
        } catch (error) {
          console.error('Error fetching hotels:', error);
        }
      }, function(error) {
        console.error('Error getting location:', error);
      });
    } else {
      console.error("Geolocation is not supported by this browser.");
    }
  };

  const handleHotelClick = async (hotelName) => {
    try {
      const response = await axios.post('http://localhost:5000/manageRooms/getHotelRooms', {
        hotelName: hotelName
      });
      setRooms(prevRooms => ({ ...prevRooms, [hotelName]: response.data }));
      setSelectedHotel(hotelName);
    } catch (error) {
      console.error('Error fetching rooms:', error);
    }
  };

  const handleBookRoom = (roomId) => {
    axios.post('http://localhost:5000/manageRooms/book', {
      username: user.username,
      roomId
    }).then(response => {
      if (response.data.success) {
        alert('Booking successfully');
        setRooms(rooms.map(room => room.roomid === roomId ? { ...room, bookedBy: user.username } : room));
      }
    }).catch(error => console.error('Error canceling booking', error));
  };

  const handleCancelBooking = (roomId) => {
    axios.post('http://localhost:5000/manageRooms/cancelBooking', {
      roomId
    }).then(response => {
      if (response.data.success) {
        alert('Booking cancelled successfully');
        
        setRooms(rooms.map(room => room.roomid === roomId ? { ...room, bookedBy: null } : room));
      }
    }).catch(error => console.error('Error canceling booking', error));
  };


  return (
    <div className="App">
      <h1>Hotel Finder</h1>
      <div className="auth-buttons">
      {user ? (
          <>
            <p>Welcome, {user.username}!</p>
            <button onClick={handleLogout}>Logout</button>
          </>
        ) : (
          <>
            <RegisterButton />
            <LoginButton setUser={setUser} />
          </>
        )}
      </div>
      <div className="input-container">
        <input
          type="number"
          value={distance}
          onChange={(e) => setDistance(e.target.value)}
          placeholder="Enter distance in km"
        />
        <button onClick={handleGetHotels}>Find Hotels</button>
      </div>
      <div className="hotels-list">
        {hotels.map((hotel) => (
          <div key={hotel.id} className="hotel" onClick={() => handleHotelClick(hotel.hotelname)}>
            <h2>{hotel.hotelname}</h2>
            <p>Latitude: {hotel.x}</p>
            <p>Longitude: {hotel.y}</p>
            {selectedHotel === hotel.hotelname && rooms[hotel.hotelname] && (
              <div className="rooms-list">
                {rooms[hotel.hotelname].map((room) => (
                  <div key={room.roomid} className="room">
                    <p>Room Number: {room.roomNumber}</p>
                    <p>Price: ${room.price}</p>
                    <p>Available: {room.isAvailable ? 'Yes' : 'No'}</p>
                    {user && (
                      room.bookedBy === user.username ? (
                        <button onClick={() => handleCancelBooking(room.roomid)}>Cancel</button>
                      ) : (
                        room.isAvailable && (
                          <button onClick={() => handleBookRoom(room.roomid)}>Book</button>
                        )
                      )
                    )}
                  </div>
                ))}
              </div>
            )}
          </div>
        ))}
      </div>
    </div>
  );
}

export default App;
