import React, { useState } from 'react';
import Modal from 'react-modal';
import axios from 'axios';

Modal.setAppElement('#root');

const RegisterButton = () => {
  const [modalIsOpen, setModalIsOpen] = useState(false);
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  const openModal = () => setModalIsOpen(true);
  const closeModal = () => setModalIsOpen(false);

  const handleRegister = async () => {
    try {
      await axios.post('http://localhost:5000/authRoutes/register', {
        username,
        password
      });
      alert('Registration successful!');
      closeModal();
    } catch (error) {
      console.error('Error registering user:', error);
      alert('Error registering user');
    }
  };

  return (
    <>
      <button onClick={openModal}>Register</button>
      <Modal
        isOpen={modalIsOpen}
        onRequestClose={closeModal}
        contentLabel="Register"
      >
        <h2>Register</h2>
        <form onSubmit={(e) => { e.preventDefault(); handleRegister(); }}>
          <input
            type="text"
            placeholder="Username"
            value={username}
            onChange={(e) => setUsername(e.target.value)}
            required
          />
          <input
            type="password"
            placeholder="Password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            required
          />
          <button type="submit">Register</button>
        </form>
        <button onClick={closeModal}>Close</button>
      </Modal>
    </>
  );
};

export default RegisterButton;
